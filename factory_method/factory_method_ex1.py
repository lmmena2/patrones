import datetime as dt
from colorama import init, Fore
from abc import ABC, abstractmethod

init(autoreset = True)

class Logger(ABC):
    
    @abstractmethod
    def info(self, message, object): ...
    
    @abstractmethod
    def warnig(self, message, object): ...
    
    @abstractmethod
    def error(self, message, object): ...
    
    @abstractmethod
    def debug(self, message, object): ...

class LoggerConsole(Logger):
    
    def info(self, message, object): 
        print (f'{dt.datetime.now()} {Fore.BLUE} [INFO] {message, object}')
    
    def warnig(self, message, object):
        print (f'{dt.datetime.now()} {Fore.YELLOW} [WARNING] {message, object}')
    
    def error(self, message, object): 
        print (f'{dt.datetime.now()} {Fore.RED} [ERROR] {message, object}')
    
    def debug(self, message, object):
        print (f'{dt.datetime.now()} {Fore.MAGENTA} [DEBUG] {message, object}')
    
log_file = 'file.log'        
class LoggerFile(Logger):
    
    def info(self, message, object): 
        with open(log_file, 'a') as file:
            data = f'{str(dt.datetime.now())} [INFO] {message, str(object)}\n'
            file.writelines(data)
    
    def warnig(self, message, object):
        with open(log_file, 'a') as file:
            data = f'{str(dt.datetime.now())} [WARNIG] {message, str(object)}\n'
            file.writelines(data)
    
    def error(self, message, object): 
        with open(log_file, 'a') as file:
            data = f'{str(dt.datetime.now())} [ERROR] {message, str(object)}\n'
            file.writelines(data)
    
    def debug(self, message, object):
        with open(log_file, 'a') as file:
            data = f'{str(dt.datetime.now())} [DEBUG] {message, str(object)}\n'
            file.writelines(data)

class LoggerEmail(Logger):
    def info(self, message, object): 
        print(f'Mail [INFO] {message, object}')
    
    def warnig(self, message, object):
        print(f'Mail [WARNIG] {message, object}')
    
    def error(self, message, object): 
        print(f'Mail [ERROR] {message, object}')
    
    def debug(self, message, object):
        print(f'Mail [DEBUG] {message, object}')
    
class LoggerFactory(ABC):
    
    @abstractmethod
    def get_logger(self, type): ...

class LoggerFacrotyImp1(LoggerFactory):
    
    def get_logger(self, type) -> Logger:
        dic = {
            'c': LoggerConsole(),
            'f': LoggerFile(),
            'e': LoggerEmail(),
        }
        
        return dic[type]

if __name__ == '__main__':
    
    type_logger = str(input("""
    [c] Para salida por consola
    [f] Para salida por archivo
    [e] Para salida por email
     >>>: """))
    
    logger = LoggerFacrotyImp1().get_logger(type = type_logger)
    logger.info('Mensaje generico', 200)
    logger.warnig('Mensaje generico', 404)
    logger.error('Mensaje generico', 401)
    logger.debug('Mensaje generico', 500)