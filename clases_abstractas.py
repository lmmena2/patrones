from abc import ABC, abstractmethod
from typing import Protocol

class Animal(ABC):
    
    @abstractmethod
    def eat(self): ...
    
    @abstractmethod
    def walk(self): ...
    
    @abstractmethod
    def sleep(self): ...
    
class Person(Animal):
    
    #overwriting
    def eat(self):
        return super().eat()
    
    def walk(self):
        return super().walk()
    
    def sleep(self):
        return super().sleep()

class Dog(Animal):
    
    def eat(self):
        return super().eat()
    
    def walk(self):
        return super().walk()
    
    def sleep(self):
        return super().sleep()
    
    
     