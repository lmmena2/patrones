from abc import ABC, abstractmethod


class Background(ABC):
    
    @abstractmethod
    def create_bg(self): ...
    
class AbstractFactory(ABC):
    
    @abstractmethod
    def create_bg(self) -> Background: ...
    
    @abstractmethod
    def create_side_bar(self): ...
    
    @abstractmethod
    def create_nav(self): ...

class DarkBaground(Background):
    
    def create_bg(self):
        return 'Se creo un fondo obscuro para la app'

class LightBaground(Background):
    
    def create_bg(self):
        return 'Se creo un fondo claro para la app'
    

class DarkThemeFactory(AbstractFactory):
    
    def create_bg(self):
        return DarkBaground()
    
    def create_side_bar(self):
        return 'Se creo una side bar obscura para la app'
    
    def create_nav(self):
        return 'Se creo una navegacion obscura para la app'
    
class LightThemeFactory(AbstractFactory):
    
    def create_bg(self):
        return LightBaground()
    
    def create_side_bar(self):
        return 'Se creo una side bar claro para la app'
    
    def create_nav(self):
        return 'Se creo una navegacion claro para la app'