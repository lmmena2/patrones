from abc import ABC, abstractmethod

class CloudStorage(ABC):
    
    @classmethod
    def show(self) -> str: ...

class Mail(ABC):
    
    @classmethod
    def show(self) -> str: ...
    
class GoogleCloudStorage(CloudStorage):
    
    def show(self) -> str:
        return 'show Google cloud storage info'

class MocrosoftCloudStorage(CloudStorage):
    
    def show(self) -> str:
        return 'show Microsoft cloud storage info'

class GoogleMail(Mail):
    
    def show(self) -> str:
        return 'show Google mail info'

class MicrosoftMail(Mail):
    
    def show(self) -> str:
        return 'show Microsoft mail info'

class AbstractFactory(ABC):
    
    @abstractmethod
    def create_cloud_storage(self) -> CloudStorage: ...
    
    @abstractmethod
    def create_mail(self) -> Mail: ...

class GoogleFactory(AbstractFactory):
    
    def create_cloud_storage(self) -> CloudStorage:
        return GoogleCloudStorage()
    
    def create_mail(self) -> Mail:
        return GoogleMail()

class MicrosoftFactory(AbstractFactory):
    
    def create_cloud_storage(self) -> CloudStorage:
        return MocrosoftCloudStorage()
    
    def create_mail(self) -> Mail:
        return MicrosoftMail()
    