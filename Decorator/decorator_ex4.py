from abc import ABC, abstractmethod

class Enemy(ABC):

    @abstractmethod
    def compute_damage(self, recive_attack: str) -> int: ...

class EnemyDecorator(Enemy):
    
    decorated_enemy: Enemy
    
    def __init__(self, enemy_to_decorate: Enemy):
        self.decorated_enemy = enemy_to_decorate
        
    def compute_damage(self, recive_attack: str) -> int: ...

def helemet_decorator(cls):
    def wrapper(*args, **kwargs):
        def new_compute_damage(self, recive_attack: str) -> int:
            if recive_attack == 'Daño a la cabeza':
                print('Enemigo con casco daño 0')
                return 0
            print('Enemigo con casco, daño 100')
            return 100
        setattr(cls, 'compute_damage', new_compute_damage)
        return cls(*args, **kwargs)
    return wrapper

def tipo_ataque() -> str:
    insert = int(input("""Selecciona el tipo de ataque
    [1] Atacar a la cabeza
    [2] Atacar al cuerpo
    [3] Atacar a las piernas
    >>>1 """))
    ataques = {
        1: 'Daño a la cabeza',
        2: 'Daño al cuerpo',
        3: 'Daño a las piernas'
    }
    return ataques[insert]

@helemet_decorator
class ConcreateEnemy(Enemy):
    
    def compute_damage(self, recive_attack: str) -> int:
        print('Enemigo desnudo, 100 de daño')
        return 100

if __name__ == '__main__':
    
    enemigo_concreto = ConcreateEnemy()
    
    enemigo_concreto = helemet_decorator(tipo_ataque())
    
    
   
        