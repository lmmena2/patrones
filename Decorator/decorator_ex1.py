

def permisos(roles: list):
    def decorador(fn):
        def wrapper(*args, **kwargs):
            if 'admin' in roles:
                return fn(*args, **kwargs)
            print({
                'Error': 401,
                "Type" : "Unathorized"
            })
        return wrapper
    return decorador 
    

@permisos(roles = ['admin', 'moderator'])
def saludar():
    return 'Hola que tal?'

func = saludar()
print (func)