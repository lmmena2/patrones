from abc import ABC, abstractmethod

class Enemy(ABC):

    @abstractmethod
    def compute_damage(self, recive_attack: str) -> int: ...
    
class ConcretateEnemy(Enemy):
    
    def compute_damage(self, recive_attack: str) -> int:
        print('100 de daño')
        return 100
    
class EnemyDecorator(Enemy):
    
    decorated_enemy: Enemy
    
    def __init__(self, enemy_to_decorate: Enemy):
        self.decorated_enemy = enemy_to_decorate
    
    def compute_damage(self, recive_attack: str) -> int: ...

class HelemetDecorator(EnemyDecorator):
    
    def compute_damage(self, recive_attack: str) -> int:
        if recive_attack == 'Daño a la cabeza':
            print('Llevo casco, no me haces daño en la cabeza')
            return 0
        print('100 de daño')
        return 100

def tipo_ataque() -> str:
    insert = int(input("""Selecciona el tipo de ataque
    [1] Atacar a la cabeza
    [2] Atacar al cuerpo
    [3] Atacar a las piernas
    """))
    ataques = {
        1: 'Daño a la cabeza',
        2: 'Daño al cuerpo',
        3: 'Daño a las piernas'
    }
    return ataques[insert]

if __name__ == '__main__':
    
    enemigo_desnudo = ConcretateEnemy()
    
    enemigo_con_casco = HelemetDecorator(enemigo_desnudo)
    
    enemigo_con_casco.compute_damage(tipo_ataque())
        